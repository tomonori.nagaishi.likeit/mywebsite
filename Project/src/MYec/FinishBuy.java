package MYec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.BuyDetailDataBeans;
import beans.ItemDataBeans;
import dao.BuyDAO;
import dao.BuyDetailDAO;

/**
 * Servlet implementation class FinishBuy
 */
@WebServlet("/FinishBuy")
public class FinishBuy extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) EcHelper.cutSessionAttribute(session, "cart");

		BuyDataBeans bdb = (BuyDataBeans) EcHelper.cutSessionAttribute(session, "bdb");

		int buyId = BuyDAO.insertBuy(bdb);

		for (ItemDataBeans cartInItem : cart) {
			BuyDetailDataBeans buydetail = new BuyDetailDataBeans();
			buydetail.setBuyId(buyId);
			buydetail.setItemId(cartInItem.getId());
			BuyDetailDAO.insertBuyDetail(buydetail);
		}


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/web-make/finishingBuy.jsp");
		dispatcher.forward(request, response);
	}

}
