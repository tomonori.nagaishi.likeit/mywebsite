package MYec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserCreate
 */
@WebServlet("/UserCreate")
public class UserCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserCreate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/web-make/userCreate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");


		String userName = request.getParameter("name");
		String userAddress = request.getParameter("address");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordConf = request.getParameter("passwordConf");


		//すでに登録されたlogin_idを検出するメソッド
        UserDataBeans user =  UserDAO.findLoginId(loginId);

		if (userName.equals("") || userAddress.equals("") || loginId.equals("") || password.equals("")
				||passwordConf.equals("")) {
			request.setAttribute("errmess", "未入力の箇所があります。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/web-make/userCreate.jsp");
			dispatcher.forward(request, response);
			return;
		} else if (!(password.equals(passwordConf))) {
			request.setAttribute("errmess", "パスワードが正しく入力されていません。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/web-make/userCreate.jsp");
			dispatcher.forward(request, response);
			return;
		} else if (user != null) {
			request.setAttribute("errmess", "既に登録されているIDです。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/web-make/userCreate.jsp");
			dispatcher.forward(request, response);
			return;

		}

		//データベースに登録するためのメソッドを利用するためのインスタンス
		UserDAO create = new UserDAO();
		create.Create(userName,userAddress,loginId,password);

		response.sendRedirect("FinishCreate");
	}



}
