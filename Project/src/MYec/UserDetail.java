package MYec;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.UserDataBeans;
import dao.BuyDAO;

/**
 * Servlet implementation class UserDetail
 */
@WebServlet("/UserDetail")
public class UserDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		UserDataBeans loginInfo = (UserDataBeans)session.getAttribute("userInfo");

		if(loginInfo == null) {

			response.sendRedirect("Login");
			return;
		}


		int id = (int) loginInfo.getId();

		BuyDAO buydao = new BuyDAO();
		List<BuyDataBeans> buyDataBeansList = buydao.findBuyDataList(id);
		request.setAttribute("buyData", buyDataBeansList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/web-make/userDetail.jsp");
		dispatcher.forward(request, response);
	}


}
