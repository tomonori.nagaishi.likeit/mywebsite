package MYec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemCategoryDataBeans;
import beans.ItemDataBeans;
import dao.ItemCategoryDAO;
import dao.ItemDAO;

/**
 * Servlet implementation class AfterSearch
 */
@WebServlet("/AfterSearch")
public class AfterSearch extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AfterSearch() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

        //カテゴリーIDから出力されたデータ

		if (request.getParameter("category_id") != null) {
			int categoryId = Integer.parseInt(request.getParameter("category_id"));
			ItemDAO ItemDAO = new ItemDAO();
			ArrayList<ItemDataBeans> itemDataBycategory = ItemDAO.FindByCategoryId(categoryId);
			request.setAttribute("itemList", itemDataBycategory);
		}

		//検索入力画面の出力
		String searchWord = request.getParameter("search_word");
		if(searchWord != null) {
			session.setAttribute("searchWord", searchWord);
			ItemDAO ItemDao = new ItemDAO();
			ArrayList<ItemDataBeans> searchResultItemList = ItemDao.getItemsByItemName(searchWord);
			request.setAttribute("itemList", searchResultItemList);
		}

		ArrayList<ItemCategoryDataBeans> itemCategoryData = ItemCategoryDAO.all();
		request.setAttribute("itemCategoryData",itemCategoryData);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/web-make/afterSearch.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
