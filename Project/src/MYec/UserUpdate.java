package MYec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserUpdate
 */
@WebServlet("/UserUpdate")
public class UserUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
        UserDataBeans logininfo=(UserDataBeans)session.getAttribute("userInfo");

        if (logininfo == null) {
			response.sendRedirect("Login");
			return;
		}

		String id = request.getParameter("id");


		UserDataBeans user = UserDAO.findUserById(id);
		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/web-make/userUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String name = request.getParameter("name");
		String address = request.getParameter("address");
		String password = request.getParameter("password");
		String passwordConf = request.getParameter("passwordConf");
		String id = request.getParameter("id");

		 if (!(password.equals(passwordConf))) {
				request.setAttribute("errmess", "パスワードが正しく入力されていません。");

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/web-make/userUpdate.jsp");
				dispatcher.forward(request, response);
				return;
		}

		UserDAO userdao = new UserDAO();
		userdao.Update(name,address,password,id);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/web-make/userUpdate.jsp");
		dispatcher.forward(request, response);


	}


}
