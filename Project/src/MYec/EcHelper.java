package MYec;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;

public class EcHelper {

	//商品計算
	public static int getTotalItemPrice(ArrayList<ItemDataBeans> items) {
		int total = 0;
		for (ItemDataBeans item : items) {
			total += item.getPrice();
		}
		return total;
	}

	//セッションから削除
	public static Object cutSessionAttribute(HttpSession session, String str) {
		Object test = session.getAttribute(str);
		session.removeAttribute(str);

		return test;
	}

}