package MYec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemCategoryDataBeans;
import beans.ItemDataBeans;
import dao.ItemCategoryDAO;
import dao.ItemDAO;

/**
 * Servlet implementation class TopPage
 */
@WebServlet("/TopPage")
public class TopPage extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public TopPage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		request.setCharacterEncoding("UTF-8");

		//アイテムカテゴリー用のリスト
		ArrayList<ItemCategoryDataBeans> itemCategoryData = ItemCategoryDAO.all();
		request.setAttribute("itemCategoryData",itemCategoryData);

        //商品リスト
		ItemDAO ItemDAO = new ItemDAO();
		ArrayList<ItemDataBeans> itemData = ItemDAO.findAll();
		request.setAttribute("itemData",itemData);

		//セッションにsearchWordが入っていたら破棄する
		String searchWord = (String)session.getAttribute("searchWord");
		if(searchWord != null) {
			session.removeAttribute("searchWord");
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/web-make/topPage.jsp");
		dispatcher.forward(request, response);

	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
