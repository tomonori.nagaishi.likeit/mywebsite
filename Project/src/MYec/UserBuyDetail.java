package MYec;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.ItemDataBeans;
import dao.BuyDAO;
import dao.BuyDetailDAO;

/**
 * Servlet implementation class UserBuyDetail
 */
@WebServlet("/UserBuyDetail")
public class UserBuyDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserBuyDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		
		int buy_id = Integer.parseInt(request.getParameter("buy_id"));

		//staticなのでクラス型変数に代入しなくて良い
        ArrayList<ItemDataBeans> itemlist = BuyDetailDAO.getItemDataBeansListByBuyId(buy_id);
        request.setAttribute("item", itemlist);



		BuyDAO data = new BuyDAO();
        List<BuyDataBeans> findBuyHistoryList = data.findBuyDataListhistory(buy_id);
        request.setAttribute("list", findBuyHistoryList);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/web-make/userBuyDetail.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
