package MYec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class CartDelete
 */
@WebServlet("/CartDelete")
public class CartDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CartDelete() {
        super();
        // TODO Auto-generated constructor stub
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
     int id = Integer.parseInt(request.getParameter("id"));

     ItemDataBeans del =ItemDAO.FindItemById(id);
     request.setAttribute("delData",del);

    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/web-make/cartDelete.jsp");
		dispatcher.forward(request, response);


    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();


			String deleteItemIdList = request.getParameter("id");
			ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");


			if (deleteItemIdList != null) {
				//削除対象の商品を削除

					for (ItemDataBeans cartInItem : cart) {
						if (cartInItem.getId() == Integer.parseInt(deleteItemIdList)) {
							cart.remove(cartInItem);
							break;
						}
					}

           }

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/web-make/cart.jsp");
			dispatcher.forward(request, response);

	}
}

