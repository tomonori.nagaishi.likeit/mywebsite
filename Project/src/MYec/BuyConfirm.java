package MYec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.DeliveryMethodDataBeans;
import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.DeliveryMethodDAO;

/**
 * Servlet implementation class BuyConfirm
 */
@WebServlet("/BuyConfirm")
public class BuyConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/web-make/buyConfirm.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		int inputDeliveryMethodId = Integer.parseInt(request.getParameter("delivery_method_id"));

		DeliveryMethodDataBeans userSelectDMB = DeliveryMethodDAO.getDeliveryMethodDataBeansByID(inputDeliveryMethodId);

		ArrayList<ItemDataBeans> cartIDBList = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

		int totalPrice = EcHelper.getTotalItemPrice(cartIDBList)+userSelectDMB.getPrice();
        
		
		BuyDataBeans bdb = new BuyDataBeans();
		UserDataBeans userInfo = (UserDataBeans)session.getAttribute("userInfo");
		bdb.setUserId((int) userInfo.getId());
		bdb.setTotalPrice(totalPrice);
		bdb.setDelivertMethodId(userSelectDMB.getId());
		bdb.setDeliveryMethodName(userSelectDMB.getName());
        bdb.setDeliveryMethodPrice(userSelectDMB.getPrice());
        
        
        session.setAttribute("bdb", bdb);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/web-make/buyConfirm.jsp");
		dispatcher.forward(request, response);
	}

}
