package beans;

import java.io.Serializable;
import java.sql.Date;




public class UserDataBeans implements Serializable {
 private int id;
 private String name;
 private String address;
 private String login_id;
 private String password;
 private Date  createDate;



  //ログインセッションを保存するためのコンストラクタ
  public UserDataBeans(String loginId, String name,String address,int id) {
		this.login_id = loginId;
		this.name = name;
		this.address = address;
		this.id = id;
	   }
  //ログインIDが重複してるか調べるためのコンストラクタ
  public UserDataBeans (String loginId) {
		this.login_id = loginId;
     }
  //更新用のコンストラクタ
  public UserDataBeans(int id, String name, String address,String loginId,  String password) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.login_id = loginId;
        this.password = password;

	}

  //全てのデータをセットするコンストラクタ
	public UserDataBeans(int id, String name, String address,String loginId,  String password, Date createDate) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.login_id = loginId;
        this.password = password;
		this.createDate = createDate;
	}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getAddress() {
	return address;
}

public void setAddress(String address) {
	this.address = address;
}

public String getLogin_id() {
	return login_id;
}

public void setLogin_id(String login_id) {
	this.login_id = login_id;
}

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}

public Date getCreateDate() {
	return createDate;
}

public void setCreateDate(Date createDate) {
	this.createDate = createDate;
}



}