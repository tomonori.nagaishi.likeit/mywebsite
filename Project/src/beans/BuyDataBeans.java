package beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BuyDataBeans  implements Serializable {

	private int id;
	private int userId;
	private int totalPrice;
	private int delivertMethodId;
	private Date buyDate;

	private String deliveryMethodName;
	private int deliveryMethodPrice;

	public BuyDataBeans() {

	}

    //ユーザー詳細で利用される購入情報のコンストラクタ
	public BuyDataBeans(int id, Date date,String deliveryMethodName, int totalPrice) {
		this.id= id;
		this.buyDate= date;
		this.deliveryMethodName= deliveryMethodName;
		this.totalPrice = totalPrice;
    }
	//購入詳細履歴コンストラクタ
	public BuyDataBeans(int id2, Date date2,String deliveryMethodName, int totalPrice2,int deliveryMethodPrice2) {
		this.id= id2;
		this.buyDate= date2;
		this.deliveryMethodName= deliveryMethodName;
		this.totalPrice = totalPrice2;
		this.deliveryMethodPrice=deliveryMethodPrice2;
		}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

	public int getDelivertMethodId() {
		return delivertMethodId;
	}

	public void setDelivertMethodId(int delivertMethodId) {
		this.delivertMethodId = delivertMethodId;
	}

	public Date getBuyDate() {
		return buyDate;
	}

	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}

	public String getDeliveryMethodName() {
		return deliveryMethodName;
	}

	public void setDeliveryMethodName(String deliveryMethodName) {
		this.deliveryMethodName = deliveryMethodName;
	}



	public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
		return sdf.format(buyDate);
	}
	public String getFormatTotalPrice() {
		return String.format("%,d", this.totalPrice);
	}
	public int getDeliveryMethodPrice() {
		return deliveryMethodPrice;
	}
	public void setDeliveryMethodPrice(int deliveryMethodPrice) {
		this.deliveryMethodPrice = deliveryMethodPrice;
	}

}