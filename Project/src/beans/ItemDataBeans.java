package beans;

import java.io.Serializable;

public class ItemDataBeans implements Serializable{
	private int id;
	private int categoryId;
	private String name;
	private int price;
	private String detail;
	private String imgFile;

	public ItemDataBeans(int id,int categoryId, String name,int price,String detail, String imgFile) {
		this.id = id;
		this.categoryId = categoryId;
		this.name = name;
		this.price = price;
		this.detail = detail;
        this.imgFile = imgFile;

	}

    public ItemDataBeans() {

	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getImgFile() {
		return imgFile;
	}
	public void setImgFile(String imgFile) {
		this.imgFile = imgFile;
	}

	public String getFormatPrice() {
		return String.format("%,d", this.price);
	}

}
