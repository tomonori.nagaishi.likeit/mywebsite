package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import DB.DBmanager;
import beans.BuyDataBeans;

public class BuyDAO {

	public static int insertBuy(BuyDataBeans bdb)  {
		Connection conn = null;
		PreparedStatement st = null;
		int autoIncKey = -1;
		try {
			conn = DBmanager.getConnection();

			st = conn.prepareStatement(
					"INSERT INTO buy(user_id,total_price,delivery_method_id,create_date) VALUES(?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			st.setInt(1, bdb.getUserId());
			st.setInt(2, bdb.getTotalPrice());
			st.setInt(3, bdb.getDelivertMethodId());
			st.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
			st.executeUpdate();

			ResultSet rs = st.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}
			System.out.println("inserting buy-datas has been completed");

			return autoIncKey;
		} catch (SQLException e) {
		      e.printStackTrace();
		  } finally {
		      // データベース切断
		      if (conn != null) {
		          try {
		              conn.close();
		          }catch (SQLException e) {
		              e.printStackTrace();
		            }
		         }
		    }
		return autoIncKey;
	}

	public  List<BuyDataBeans> findBuyDataList(int ID) {
        Connection conn = null;
        PreparedStatement st = null;
        List<BuyDataBeans> buyDataBeansList = new ArrayList<BuyDataBeans>();

        try {
            // データベースへ接続
            conn = DBmanager.getConnection();


            // SELECT文を準備
            // TODO: 未実装：管理者以外を取得するようSQLを変更する
            st = conn.prepareStatement(
            		" Select buy.id,buy.create_date,delivery_method.name,buy.total_price"
            		      + " FROM buy INNER JOIN delivery_method "
            		      + " ON buy.delivery_method_id = delivery_method.id "
            		      + " WHERE buy.user_id = ? "
            		      + " ORDER BY buy.create_date DESC");

            		      st.setInt(1, ID);
             // SELECTを実行し、結果表を取得
            ResultSet rs = st.executeQuery();

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
            	Timestamp date = rs.getTimestamp("create_date");
                String name = rs.getString("name");
                int totalPrice = rs.getInt("total_price");


                BuyDataBeans buyDataBeans = new BuyDataBeans(id, date , name,totalPrice);

                buyDataBeansList.add(buyDataBeans);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return buyDataBeansList;
    }

	 public List<BuyDataBeans> findBuyDataListhistory(int buy_id) {
	        Connection conn = null;
	        PreparedStatement st = null;
	        List<BuyDataBeans> findBuyDataListhistory = new ArrayList<BuyDataBeans>();

	        try {
	            // データベースへ接続
	            conn = DBmanager.getConnection();


	            // SELECT文を準備

	            st = conn.prepareStatement(
	            		" Select buy.id,buy.create_date,delivery_method.name,buy.total_price,delivery_method.price "
	            		      + " FROM buy INNER JOIN delivery_method "
	            		      + " ON buy.delivery_method_id = delivery_method.id "
	            		      + " WHERE buy.id = ? ");

	            		      st.setInt(1, buy_id);
	             // SELECTを実行し、結果表を取得
	            ResultSet rs = st.executeQuery();

	            // 結果表に格納されたレコードの内容を
	            // Userインスタンスに設定し、ArrayListインスタンスに追加
	            while (rs.next()) {
	                int id = rs.getInt("id");
	            	Timestamp date = rs.getTimestamp("create_date");
	                String deliveryMethodName = rs.getString("name");
	                int totalPrice = rs.getInt("total_price");
	                int deliveryMethodPrice = rs.getInt("price");
	                BuyDataBeans buyDataBeans = new BuyDataBeans(id, date , deliveryMethodName, totalPrice , deliveryMethodPrice);

	                findBuyDataListhistory .add(buyDataBeans);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	        return findBuyDataListhistory ;
	    }


}