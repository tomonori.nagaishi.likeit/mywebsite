package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.bind.DatatypeConverter;

import DB.DBmanager;
import beans.UserDataBeans;


public class UserDAO {




	//１.新規登録するためのメソッド
	public void Create(String userName,String userAddress, String loginId, String password) {
	        Connection conn = null;
	        String pass = algo(password);

	        try {
	            // データベースへ接続
	        	conn = DBmanager.getConnection();

	            // SELECT文を準備
	            String sql = "INSERT INTO user(name,address,login_id,password,create_date)VALUES(?,?,?,?,now())";

	            PreparedStatement pStmt = conn.prepareStatement(sql);
	            pStmt.setString(1, userName);
	            pStmt.setString(2, userAddress);
	            pStmt.setString(3, loginId);
	            pStmt.setString(4, pass);
	            int rs = pStmt.executeUpdate();





	        } catch (SQLException e) {
	            e.printStackTrace();
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
	    }


    //2.ログインIDが重複しているかどうか確かめるメソッド
	public static UserDataBeans findLoginId (String loginId){
        Connection conn = null;

        try {
            // データベースへ接続
        	conn = DBmanager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ? ";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);

            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");

            return new UserDataBeans(loginIdData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }


    //ログインのためのメソッド
	public static UserDataBeans findByLoginInfo(String loginId, String password) {
        Connection conn = null;
        String pass = algo(password);

        try {
            // データベースへ接続
        	conn = DBmanager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, pass);
            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");//login_idはSQLのカラム名
            String nameData = rs.getString("name");
            String address = rs.getString("address");
            int id =rs.getInt("id");
            return new UserDataBeans(loginIdData, nameData,address,id);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }
	//ログインユーザーのidから検出し該当ユーザーの情報を出力するメソッド
    public static UserDataBeans findUserById(String id) {
        Connection conn = null;

        try {
            // データベースへ接続
        	conn = DBmanager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user where id = ? ";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, id);

            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
             int IdData = rs.getInt("id");
             String name = rs.getString("name");
             String Address = rs.getString("address");
             String loginId = rs.getString("login_id");
             String Password = rs.getString("password");
             Date CreateDate=rs.getDate("create_data");


            return  new UserDataBeans(IdData,name,Address,loginId,Password,CreateDate);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }


    //ユーザー更新メソッド
	public void Update(String name,String address, String password,String id)  {
        Connection conn = null;
        String pass = algo(password);

        try {
            // データベースへ接続
        	conn = DBmanager.getConnection();


            // SELECT文を準備
            String sql = "update user set name=?,address=?,password=? where id = ? ;" ;

            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, name);
            pStmt.setString(2, address);
            pStmt.setString(3, pass);
            pStmt.setString(4, id);



            int rs = pStmt.executeUpdate();





        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }



	//暗号化するためのメソッド
	public static String algo(String password) {
    	String source = password;
    	//ハッシュ生成前にバイト配列に置き換える際のCharset
    	Charset charset = StandardCharsets.UTF_8;
    	//ハッシュアルゴリズム
    	String algorithm = "MD5";

    	//ハッシュ生成処理
    	byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
    	String result = DatatypeConverter.printHexBinary(bytes);
    	//標準出力

       return result;
    }



}