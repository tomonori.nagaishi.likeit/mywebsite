package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import DB.DBmanager;
import beans.ItemCategoryDataBeans;

public class ItemCategoryDAO{

	//カテゴリー検索用の
	public static ArrayList<ItemCategoryDataBeans> all(){
	        Connection conn = null;
	        ArrayList<ItemCategoryDataBeans> categoryList = new ArrayList<ItemCategoryDataBeans>();

	        try {
	            // データベースへ接続
	            conn = DBmanager.getConnection();

	            // SELECT文を準備

	            String sql = "SELECT * FROM item_category";

	            //String sql = "SELECT * FROM user ";
	            //ログイン後管理者ユーザーを出力する場合はこのSQL文でOK

	             // SELECTを実行し、結果表を取得
	            Statement stmt = conn.createStatement();
	            ResultSet rs = stmt.executeQuery(sql);

	            // 結果表に格納されたレコードの内容を
	            // Userインスタンスに設定し、ArrayListインスタンスに追加
	            while (rs.next()) {
	                int id = rs.getInt("id");
	                String name = rs.getString("name");


	                ItemCategoryDataBeans item = new ItemCategoryDataBeans(id,name);

	                categoryList.add(item);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	        return categoryList;
	    }




}