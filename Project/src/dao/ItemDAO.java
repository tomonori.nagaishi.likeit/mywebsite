package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import DB.DBmanager;
import beans.ItemDataBeans;

 //商品登録メソッド
 public class ItemDAO{

	public void ItemCreate(int categoryId,String itemName,int itemPrice, String itemDetail, String itemImg) {
        Connection conn = null;


        try {
            // データベースへ接続
        	conn = DBmanager.getConnection();

            // SELECT文を準備
            String sql = "INSERT INTO item(category_id,name,price,detail,img_file) VALUES(?,?,?,?,?)";

            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setInt(1, categoryId);
            pStmt.setString(2, itemName);
            pStmt.setInt(3, itemPrice);
            pStmt.setString(4, itemDetail);
            pStmt.setString(5, itemImg);
            int rs = pStmt.executeUpdate();





        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

	//全ての商品を表示する
	 public ArrayList<ItemDataBeans> findAll() {
	        Connection conn = null;
	        ArrayList<ItemDataBeans> ItemList = new ArrayList<ItemDataBeans>();

	        try {
	            // データベースへ接続
	            conn = DBmanager.getConnection();

	            String sql = "SELECT * FROM item";

	             // SELECTを実行し、結果表を取得
	            Statement stmt = conn.createStatement();
	            ResultSet rs = stmt.executeQuery(sql);

	            while (rs.next()) {
	                int id = rs.getInt("id");
	                int categoryId = rs.getInt("category_id");
	                String name = rs.getString("name");
	                int Price = rs.getInt("price");
	                String Detail = rs.getString("detail");
	                String ItemImg= rs.getString("img_file");

	                ItemDataBeans item = new ItemDataBeans(id, categoryId, name, Price, Detail, ItemImg);

	                ItemList.add(item);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	        return ItemList;
	    }

	 //商品カテゴリーで商品を表示する
	 public ArrayList<ItemDataBeans> FindByCategoryId(int categoryId) {
	        Connection conn = null;
	        ArrayList<ItemDataBeans> ItemList = new ArrayList<ItemDataBeans>();

	        try {
	            // データベースへ接続
	            conn = DBmanager.getConnection();

	            // SELECT文を準備

	            String sql = "SELECT * FROM item where category_id = ?";

	            // SELECTを実行し、結果表を取得
	            PreparedStatement pStmt = conn.prepareStatement(sql);
	            pStmt.setInt(1, categoryId);

	            ResultSet rs = pStmt.executeQuery();



	            // 結果表に格納されたレコードの内容を
	            // Userインスタンスに設定し、ArrayListインスタンスに追加
	            while (rs.next()) {
	                int id = rs.getInt("id");
	                int categoryID = rs.getInt("category_id");
	                String name = rs.getString("name");
	                int Price = rs.getInt("price");
	                String Detail = rs.getString("detail");
	                String ItemImg= rs.getString("img_file");

	                ItemDataBeans item = new ItemDataBeans(id, categoryID, name, Price, Detail, ItemImg);

	                ItemList.add(item);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	        return ItemList;
	    }
	 //検索フォーム入力によって商品を表示する
	 public static ArrayList<ItemDataBeans> getItemsByItemName(String searchWord)  {
			Connection conn = null;
			PreparedStatement st = null;
			ArrayList<ItemDataBeans> ItemList = new ArrayList<ItemDataBeans>();
			try {
				conn = DBmanager.getConnection();

				if (searchWord.length() == 0) {
					// 全検索
					st = conn.prepareStatement("SELECT * FROM item");

				} else {
					// 商品名検索
					st = conn.prepareStatement("SELECT * FROM item WHERE name LIKE'%"+searchWord+"%'");

				}

				ResultSet rs = st.executeQuery();

				while (rs.next()) {
	                int id = rs.getInt("id");
	                int categoryID = rs.getInt("category_id");
	                String name = rs.getString("name");
	                int Price = rs.getInt("price");
	                String Detail = rs.getString("detail");
	                String ItemImg= rs.getString("img_file");

	                ItemDataBeans item = new ItemDataBeans(id, categoryID, name, Price, Detail, ItemImg);

	                ItemList.add(item);
	            }

				System.out.println("get Items by itemName has been completed");

			} catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	        return ItemList;
	    }

	 //商品IDによって商品データを表示する。（主に商品詳細で利用）
	 public static ItemDataBeans FindItemById(int id) {
	        Connection conn = null;

	        try {
	            // データベースへ接続
	        	conn = DBmanager.getConnection();

	            // SELECT文を準備
	            String sql = "SELECT * FROM item where id = ? ";

	             // SELECTを実行し、結果表を取得
	            PreparedStatement pStmt = conn.prepareStatement(sql);
	            pStmt.setInt(1, id);

	            ResultSet rs = pStmt.executeQuery();


	            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	            if (!rs.next()) {
	                return null;
	            }

	            // 必要なデータのみインスタンスのフィールドに追加
	            int IdData = rs.getInt("id");
	              int categoryId = rs.getInt("category_id");
	              String name = rs.getString("name");
	              int price = rs.getInt("price");
	              String detail = rs.getString("detail");
	              String ImgFile = rs.getString("img_file");


	            return  new ItemDataBeans(IdData,categoryId,name,price,detail,ImgFile);
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	    }



	 //商品削除
	 public void ItemDelete(int id) {
	    	Connection conn = null;

	    try {
      // データベースへ接続
  	conn = DBmanager.getConnection();

      // SELECT文を準備
      String sql = "Delete from item where id = ? ";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);

      int rs = pStmt.executeUpdate();

  } catch (SQLException e) {
      e.printStackTrace();
  } finally {
      // データベース切断
      if (conn != null) {
          try {
              conn.close();
          }catch (SQLException e) {
              e.printStackTrace();
            }
         }
    }
 }



}