<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <!DOCTYPE html>

<html lang="ja">

<head>
   <meta charset="utf-8"/>

      <title>購入画面</title>


<!--boostrapを取り込むコード !-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!--fontawesomeを取り込むコード !-->
<link href="https://use.fontawesome.com/releases/v5.12.0/css/all.css" rel="stylesheet">

</head>

<body>

    <header>
       <nav class="navbar navbar-expand-sm sticky-top navbar-dark bg-dark my-3">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav4" aria-controls="navbarNav4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
            <a class="navbar-brand ml-5 pl-5 " href="TopPage">ECショップ</a>
            <div class="collapse navbar-collapse justify-content-end">
                <ul class="navbar-nav">

                    <li class="nav-item">
                        <a class="nav-link mr-2 " href="Cart"><i class="fas fa-shopping-cart"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mr-5" href="UserDetail"><i class="fas fa-user"></i></a>
                    </li>
                </ul>
            </div>　
        </nav>
    </header>

<div class ="container">
 <form action="BuyConfirm" method="POST">
    <div class="card-content">
　
            <div class="row">
				<table class="table">
                   <thead>
                        <tr>
                            <th colspan="2" style=text-align:center;>商品名</th>
                            <th style=text-align:center;>単価</th>

                        </tr>
                   </thead>
                      <tbody>
                      <c:forEach var="cartInItem" items="${cart}" >
                          <tr>

                              <th style=text-align:center;>${cartInItem.name}<th>
                              <th style=text-align:right;>${cartInItem.price}円

                         </tr>
                        </c:forEach>
                     </tbody>

                      <thead>
	                      <tr>
	                      <th colspan="4" style=text-align:center; >配送方法</th>
                      </thead>
                      <tbody>
                          <tr>
                            <th colspan="4" style = text-align:right;>

                                  <select name="delivery_method_id">
                                   <c:forEach var="Dmethod" items="${dmdbList}" >
								    <option value="${Dmethod.id}">${Dmethod.name}</option>
								    </c:forEach>
								   </select>
							</th>
                          </tr>
                      </tbody>
                 </table>

                       <!--
                              <th  style=text-align:right;>

                                  <select name="delivery_method_id">
                                   <c:forEach var="Dmethod" items="${dmdbList}" >
								    <option value="${Dmethod.id}">${Dmethod.name}</option>
								    </c:forEach>
								   </select>

                              </th>
                        !-->

             </div>

    </div>
		    <div class="submit-button-area" style="text-align:center;">
		       <button  class="btn btn-success btn-lg" type="submit" name="action">購入確認</button>
		    </div>
</form>
</div><!--/container!-->

    <footer>
     <p class="fixed-bottom" style="text-align: center;"> © 2020 EC </p>
    </footer>

    </body>
</html>
