<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html lang="ja">

<head>
   <meta charset="utf-8"/>

      <title>購入確認画面</title>
<!--CSSファイルを取り込むコード !-->
<!--<link href="common.css" rel="stylesheet" >!-->

<!--boostrapを取り込むコード !-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!--fontawesomeを取り込むコード !-->
<link href="https://use.fontawesome.com/releases/v5.12.0/css/all.css" rel="stylesheet">

</head>

<body>

<header>
       <nav class="navbar navbar-expand-sm sticky-top navbar-dark bg-dark my-3">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav4" aria-controls="navbarNav4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand ml-5 pl-5 " href="TopPage">ECショップ</a>
        <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">

                <li class="nav-item">
                    <a class="nav-link mr-2 " href="Cart"><i class="fas fa-shopping-cart"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mr-5" href="UserDetail"><i class="fas fa-user"></i></a>
                </li>
            </ul>
        </div>　
    </nav>
</header>

<div class="container">

    <h2 style = "text-align: center;">購入確認画面</h2>


<div class="table-responsive">
 <table class="table" >
  <thead>
    <tr>

      <th scope="col" colspan="4" style="text-align: center;">購入商品</th>
      <th scope="col" colspan="1"style="text-align: center;">金額</th>
    </tr>
  </thead>
  <tbody>
  <c:forEach var="cartInItem" items="${cart}" >
    <tr>
     <td colspan="4"style="height:100px; text-align:center;">${cartInItem.name}</td>
     <td style="text-align:right;">${cartInItem.formatPrice}円</td>
    </tr>
 </c:forEach>

    <tr>
      <td colspan="4" style="text-align:center;">${bdb.deliveryMethodName}</td>
      <td style="text-align:right;">${bdb.deliveryMethodPrice}円</td>
    </tr>

    <tr>
      <td></td>
      <td></td>
      <td colspan="2" style="text-align:right;">合計</td>
      <td colspan="2" style="text-align:right;">${bdb.formatTotalPrice}円</td>
    </tr>

  </tbody>
 </table>
</div>
    <div class="submit-button-area" style="text-align:center;">
    <form action="FinishBuy" method="post">
         <button class="btn btn-success btn-lg " type="submit" style=width:200px;>購入</button>
     </form>
    </div>
　</div><!--container!-->
    <footer class="fixed-bottom">
     <p style="text-align: center;"> © 2020 EC </p>
    </footer>
 </body>
</html>
