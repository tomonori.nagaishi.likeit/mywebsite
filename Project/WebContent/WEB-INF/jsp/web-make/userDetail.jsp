<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>ユーザー詳細</title>
 <!-- BootstrapのCSS読み込み -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<!--fontawesomeを取り込むコード !-->
<link href="https://use.fontawesome.com/releases/v5.12.0/css/all.css" rel="stylesheet">

  <!-- オリジナルCSS読み込み -->
  <link href="css/common.css" rel="stylesheet">

</head>
<body>
<header>
       <nav class="navbar navbar-expand-sm sticky-top navbar-dark bg-dark my-3">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav4" aria-controls="navbarNav4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand ml-5 pl-5 " href="TopPage">ECショップ</a>
        <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
            <c:if test="${userInfo.login_id == '管理者'}">
                <li class="nav-item active">
                    <a class="nav-link mr-2" href="ItemCreate">商品登録</a>
                </li>
           </c:if>
                <li class="nav-item active">
                    <a class="nav-link mr-2" href="Logout"><i class="fa fa-sign-out-alt fa-border fa-lg fa-spin"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mr-2 " href="Cart"><i class="fas fa-shopping-cart"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mr-5" href="UserDetail"><i class="fas fa-user"></i></a>
                </li>
            </ul>
        </div>　
    </nav>
</header>
<div class="container">

    <div class="row　col">
         <div class="table-responsice">
              <table class="table table-bordered">
                  <thead class="thead-dark ">
                      <tr>
                          <th style="width:100px;"></th>
                          <th style="text-align:center;">ユーザー情報</th>

                      </tr>
                       <tr>
                          <td>名前</td>
                          <td>${userInfo.name}</td>
                       </tr>
                       <tr>
                        <td >住所</td>
                        <td>${userInfo.address}</td>
                       </tr>
                       <tr>
                        <td>ログインID</td>
                        <td>${userInfo.login_id}</td>
                       </tr>
                  </thead>

              </table>


         </div>
        <div class="row col-12 justify-content-center">
　　    <div class="col-md-4 ">
　　　     <a href="UserUpdate?id=${userInfo.id}" class="btn btn-success btn-lg ">登録内容の変更はこちら</a>
       </div>
    </div>


　



    <div class="row　col">
        <h3>購入履歴</h3>
         <div class="table-responsice">
              <table class="table table-bordered">
                  <thead class="thead-dark">
                      <tr>
                          <th style="width:10px;"></th>
                          <th >購入日時</th>
                          <th>配送方法</th>
                          <th style="text-align:center;">購入金額</th>
                      </tr>
                       <c:forEach var="buy" items="${buyData}" >
                       <tr>
                          <td><a href="UserBuyDetail?buy_id=${buy.id}"><i class="fas fa-plus-circle fa-2x  fa-border" style="color:darkgreen;"></i></a></td>
                          <td>${buy.formatDate}</td>
                          <td>${buy.deliveryMethodName}</td>
                          <td style="text-align:right;">${buy.totalPrice}円</td>
                      </tr>
                      </c:forEach>

                  </thead>

              </table>

          </div>

        </div>
　　　
     </div>

</div> <!--container !-->
    <footer >
     <p class="fixed-bottom" style="text-align: center;"> © 2020 EC </p>
    </footer>
　</body>
</html>