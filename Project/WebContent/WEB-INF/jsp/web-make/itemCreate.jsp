<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>商品登録画面</title>
  <!-- BootstrapのCSS読み込み -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<!--fontawesomeを取り込むコード !-->
<link href="https://use.fontawesome.com/releases/v5.12.0/css/all.css" rel="stylesheet">

  <!-- オリジナルCSS読み込み -->
  <link href="css/common.css" rel="stylesheet">

</head>

<body>

  <!-- header -->
 <header>
       <nav class="navbar navbar-expand-sm sticky-top navbar-dark bg-dark my-3">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav4" aria-controls="navbarNav4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand ml-5 pl-5 " href="TopPage">ECショップ</a>
        <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link mr-2" href="ItemCreate">商品登録</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mr-2 " href="Cart"><i class="fas fa-shopping-cart"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mr-5" href="UserDetail"><i class="fas fa-user"></i></a>
                </li>
            </ul>
        </div>　
    </nav>
</header>
  <!-- /header -->

  <!-- body -->


<div class="container">



    <form method="post" action="ItemCreate" class="form-group form-horizontal " enctype="multipart/form-data">

    <div class="form-group row">
        <label for="itemName" class="col-sm-2  col-form-label">商品名</label>
        <div class="col-md-8">
          <input type="text" name="itemName"class="form-control" rows ="10" id="itemName">
        </div>
      </div>

   <div class="form-group row">
     <label for="itemCategory" class="col-sm-2  col-form-label">商品カテゴリー</label>
     <div class="col-sm-2">
     <select class="form-control" name="categoryId" id="itemCategory">
      <option  value="1">1 　コーヒー</option>
      <option  value="2">2　 お茶</option>
      <option  value="3">3 　水</option>
     </select>
     </div>
   </div>

      <div class="form-group row">
        <label for="itemPrice" class="col-sm-2 col-form-label">商品値段</label>
        <div class="col-sm-2">
          <input type="price" name="itemPrice" class="form-control" id="itemPrice">
        </div>
          <div class="col-sm-1 " style="margin-left:-20px; margin-top:5px;">
          <p>円</p>
        </div>
      </div>

      <div class="form-group row">
        <label for="itemImage" class="col-sm-2 col-form-label">商品画像</label>
        <div class="col-sm-10">
          <input type="file" name="itemImage" id="itemImage">
        </div>
      </div>


     <div class="form-group row">
        <label for="itemDetail" class="col-sm-2 col-form-label">商品詳細</label>
        <div class="col-sm-10">
          <textarea rows="5" name="itemDetail" class="form-control"  id="itemExplain"></textarea>
        </div>
      </div>

      <div class="submit-button-area">
        <button type="submit" value="検索" class="btn btn-primary btn-lg btn-block">商品登録</button>
      </div>


    </form>


  </div>



<footer>
     <p class="fixed-bottom" class="fixed-bottom"style="text-align: center;"> © 2020 EC </p>
</footer>


</body>

</html>