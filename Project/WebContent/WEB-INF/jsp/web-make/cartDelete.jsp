<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>CART削除画面</title>
  <!-- BootstrapのCSS読み込み -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <!-- オリジナルCSS読み込み -->
  <link href="css/original/common.css" rel="stylesheet">
<!--fontawesomeを取り込むコード !-->
<link href="https://use.fontawesome.com/releases/v5.12.0/css/all.css" rel="stylesheet">


</head>

<body>

  <!-- header -->
 <header>
       <nav class="navbar navbar-expand-sm sticky-top navbar-dark bg-dark my-3">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav4" aria-controls="navbarNav4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand ml-5 pl-5 " href="TopPage">ECショップ</a>
        <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">

                <li class="nav-item">
                    <a class="nav-link mr-2 " href="Cart"><i class="fas fa-shopping-cart"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mr-5" href="UserDetail"><i class="fas fa-user"></i></a>
                </li>
            </ul>
        </div>　
    </nav>
</header>
  <!-- /header -->

  <!-- body -->
  <div class="container">
    <div class="delete-area">

        <div class="text-center pr-5">
         Cartから商品を消去しますか？
        </div>

 <form action="CartDelete" method="post">
 <input type="hidden" name="id" value="${delData.id}">
      <div class="row">

        <div class="w-25 p-3 ml-5">
          <a href="Cart" class="btn btn-light btn-block">いいえ</a>
        </div>

          <div class="w-25 p-3 offset-md-5">
           <button type="submit" class="btn btn-danger  btn-block ">はい</button>
        </div>
       </div>
  </form>
    </div>

  <div class="row">

   <div class="card" style="width: 12rem;">
    <img src="img/${delData.imgFile}" class="card-img-top"alt="...">
          <div class="card-body">
            <div class="row"style="text-align:center;">
            <p class="card-text">${delData.name}</p>
            </div>

            <div class="row" style=" margin-top:50px; margin-left:120px;">
            <p>${delData.price}円</p>
            </div>

         </div>
    </div>
  </div>

 </div><!--/container!-->　

    <footer >
     <p class="fixed-bottom" style="text-align: center;"> © 2020 EC </p>
    </footer>
</body>

</html>