<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>ログイン画面</title>
  <!-- BootstrapのCSS読み込み -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <!-- オリジナルCSS読み込み -->
  <link href="css/login.css" rel="stylesheet">
<!--fontawesomeを取り込むコード !-->
<link href="https://use.fontawesome.com/releases/v5.12.0/css/all.css" rel="stylesheet">


</head>

<body>

  <!-- header -->
 <header>
       <nav class="navbar navbar-expand-sm sticky-top navbar-dark bg-dark my-3">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav4" aria-controls="navbarNav4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand ml-5 pl-5 " href="TopPage">ECショップ</a>
        <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
                <c:if test="${userInfo.login_id == '管理者'}">
	                <li class="nav-item active">
	                    <a class="nav-link mr-2" href="ItemCreate">商品登録</a>
	                </li>
               </c:if>
               <c:if test="${userInfo == null}">
	                <li class="nav-item active">
	                    <a class="nav-link mr-2" href="UserCreate">新規登録</a>
	                </li>
                </c:if>
                 <c:if test="${userInfo != null}">
	                <li class="nav-item">
	                    <a class="nav-link mr-2 " href="Cart"><i class="fas fa-shopping-cart"></i></a>
	                </li>
                 </c:if>
                <li class="nav-item">
                    <a class="nav-link mr-5" href="UserDetail"><i class="fas fa-user"></i></a>
               </li>
            </ul>
        </div>　
    </nav>
</header>
  <!-- /header -->

  <!-- body -->
<!--ログインフォームフォーマット !-->
    <form method="post" action="Login">
     <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
        <div class="container login-container">
                <div class="col-md-6 login-form-1">
                    <h3>ログイン画面</h3>

                        <div class="form-group">
                            <input type="text" name="loginId" class="form-control" placeholder="Your ID " value="" />
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Your Password " value="" />
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btnSubmit" value="ログイン" />
                        </div>
                        <div class="form-group">
                            <a href="UserCreate" class="addPwd">新規登録</a>
                        </div>

                </div>
        </div>
     </form>

 <footer>
     <p class="fixed-bottom"style="text-align: center;"> © 2020 EC </p>
    </footer>
</body>

</html>