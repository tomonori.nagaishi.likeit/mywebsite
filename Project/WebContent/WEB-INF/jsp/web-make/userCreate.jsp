<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>ユーザ新規登録画面</title>
  <!-- BootstrapのCSS読み込み -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<!--fontawesomeを取り込むコード !-->
<link href="https://use.fontawesome.com/releases/v5.12.0/css/all.css" rel="stylesheet">

  <!-- オリジナルCSS読み込み -->


</head>

<body>

  <!-- header -->
   <header>
       <nav class="navbar navbar-expand-sm sticky-top navbar-dark bg-dark my-3">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav4" aria-controls="navbarNav4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand ml-5 pl-5 " href="TopPage">ECショップ</a>
        <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">

                <li class="nav-item active">
                    <a class="nav-link mr-2" href="UserCreate">新規登録</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mr-2 " href="Cart"><i class="fas fa-shopping-cart"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mr-5" href="UserDetail"><i class="fas fa-user"></i></a>
                </li>
            </ul>
        </div>　
    </nav>
</header>
<!-- header -->
  <!-- body -->







    <div class="container">

   <!-- エラーメッセージのサンプル(エラーがある場合のみ表示) -->
   <c:if test="${errmess!=null}">
   <div class="alert alert-danger" role="alert" style="text-align: center">${errmess}</div>
   </c:if>

    <form method="post" action="UserCreate" class="form-horizontal mt-5 pl-5 ml-4">

    <div class="form-group row">
        <label for="name" class="col-sm-2 col-form-label">名前</label>
        <div class="col-sm-10">
          <input type="text" name="name" class="form-control" id="name">
        </div>
      </div>

      <div class="form-group row">
        <label for="address" class="col-sm-2 col-form-label">住所</label>
        <div class="col-sm-10">
          <input type="text" name="address" class="form-control" id="address">
        </div>
      </div>

      <div class="form-group row">
        <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
        <div class="col-sm-10">
          <input type="text" name="loginId" class="form-control" id="loginId">
        </div>
      </div>

      <div class="form-group row">
        <label for="password" class="col-sm-2 col-form-label">パスワード</label>
        <div class="col-sm-10">
          <input type="password" name="password" class="form-control" id="password">
        </div>
      </div>

      <div class="form-group row">
        <label for="passwordConf" class="col-sm-2 col-form-label">パスワード（確認）</label>
        <div class="col-sm-10">
          <input type="password" name="passwordConf" class="form-control" id="passwordConf">
        </div>
      </div>

      <div class="row justify-content-center">
      <div class="submit-button-area col-10 mt-2">
        <button type="submit" value="検索" class="btn btn-primary btn-lg btn-block">登録</button>
      </div>
    </div>

      <div class="col-xs-4">
        <a href="userList.html">戻る</a>
      </div>

     </form>

   </div>




   <footer >
     <p class="fixed-bottom" style="text-align: center;"> © 2020 EC </p>
   </footer>

 </body>

</html>