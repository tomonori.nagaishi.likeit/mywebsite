<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <!DOCTYPE html>

<html lang="ja">

<head>
<meta charset="utf-8"/>

  <title>商品詳細画面</title>
<!--CSSファイルを取り込むコード !-->
<!--<link href="common.css" rel="stylesheet" >!-->

<!--boostrapを取り込むコード !-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<!--fontawesomeを取り込むコード !-->
<link href="https://use.fontawesome.com/releases/v5.12.0/css/all.css" rel="stylesheet">

</head>

<body>

<header>
       <nav class="navbar navbar-expand-sm sticky-top navbar-dark bg-dark my-3">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav4" aria-controls="navbarNav4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand ml-5 pl-5 " href="TopPage">ECショップ</a>
        <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
                <c:if test="${userInfo.login_id == '管理者'}">
	                <li class="nav-item active">
	                    <a class="nav-link mr-2" href="ItemCreate">商品登録</a>
	                </li>
               </c:if>
               <c:if test="${userInfo == null}">
	                <li class="nav-item active">
	                    <a class="nav-link mr-2" href="UserCreate">新規登録</a>
	                </li>
                </c:if>
                 <c:if test="${userInfo != null}">
	                <li class="nav-item">
	                    <a class="nav-link mr-2 " href="Cart"><i class="fas fa-shopping-cart"></i></a>
	                </li>
                 </c:if>
                <li class="nav-item">
                    <a class="nav-link mr-5" href="UserDetail"><i class="fas fa-user"></i></a>
               </li>
            </ul>
        </div>　
    </nav>
</header>
    <div class="text-center">
     <h3>商品詳細画面</h3>

    </div>


<div class="container" style="margin-right:100px;">
  <div class="row">
    <div class="col">
      <img src="img/${item.imgFile}" alt="" width= "500" align="left" class="img-thumbnail">
        <div class="text-center">
        <h4>商品名</h4>
            <p>${item.name}</p>
       </div>

       <div class="text-center">
        <h4>商品説明</h4>
         <p>${item.detail}</p>
       </div>


       <div class="text-center mt-5">
        <h4>商品値段</h4>
        <p style="text-align:right; margin-top: 5;">${item.price}円</p>
      </div>
      <c:if test="${userInfo.login_id=='管理者'}">
        <div class="submit-button-area " style="text-align: right;" >
             <a href="ItemDelete?id=${item.id}" class="btn btn-danger  btn-lg " style="width:100px; margin-top:50px;" >削除</a>
      </div>
      </c:if>
    </div>
  </div>
</div>


    <div class="submit-button-area" style="text-align:center;">
         <a href="Cart?id=${item.id}" class="btn btn-success btn-lg " style=width:200px;>カートに入れる</a>
    </div>


    <footer>
     <p class="fixed-bottom" style="text-align: center;"> © 2020 EC </p>
    </footer>
    </body>
</html>
