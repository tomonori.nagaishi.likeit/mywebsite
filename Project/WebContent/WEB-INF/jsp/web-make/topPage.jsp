<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <!DOCTYPE html>

<html lang="ja">

<head>
   <meta charset="utf-8"/>

      <title>トップページ</title>


<!--boostrapを取り込むコード !-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!--fontawesomeを取り込むコード !-->
<link href="https://use.fontawesome.com/releases/v5.12.0/css/all.css" rel="stylesheet">

</head>

<body>

<header>
       <nav class="navbar navbar-expand-sm sticky-top navbar-dark bg-dark my-3">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav4" aria-controls="navbarNav4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand ml-5 pl-5 " href="#">ECショップ</a>
        <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
　　　　　　　　　<c:if test="${userInfo.login_id == '管理者'}">
                <li class="nav-item active">
                    <a class="nav-link mr-2" href="ItemCreate">商品登録</a>
                </li>
               </c:if>
               <c:if test="${userInfo == null}">
                <li class="nav-item active">
                    <a class="nav-link mr-2" href="UserCreate">新規登録</a>
                </li>
                </c:if>
                 <c:if test="${userInfo != null}">
                <li class="nav-item">
                    <a class="nav-link mr-2 " href="Cart"><i class="fas fa-shopping-cart"></i></a>
                </li>
                 </c:if>
                <li class="nav-item">
                    <a class="nav-link mr-5" href="UserDetail"><i class="fas fa-user"></i></a>
                </li>
            </ul>
        </div>　
    </nav>
</header>


<div class="container-fluid">
　　

   <!--検索画面!-->
  <form action="AfterSearch">
	  <div class="row justify-content-center" >
		 <div class="input-group col-sm-5" >
		　　 <input type="text" name="search_word" class="form-control" placeholder="検索" aria-label="検索" aria-describedby="button-addon" >
			      <div class="input-group-append">
			       <button type="button" id="button-addon" class="btn btn-outline-primary"><i class="fas fa-search"></i></button>
			      </div>
		 </div>
	  </div>
 </form>
<!--検索画面!-->
　
<div class="row">
<!-- サイドメニュー!-->
   <div class="col-lg-3">

    <div id="list-example" class="list-group" style="max-width: 300px;">
     <a class="list-group-item list-group-item-action" style="height:auto; text-align: center" href="#">商品カテゴリー</a>
     <c:forEach var="itemCategory" items="${itemCategoryData}">
     <a class="list-group-item list-group-item-action" href="AfterSearch?category_id=${itemCategory.id}">${itemCategory.name}</a>
     </c:forEach>
    </div>
   </div>

<!--サイドメニュー !-->

<!--商品一覧画面!-->
<div class="col-lg-9 col-sm-9">
   <div class="row">
 <c:forEach var="item" items="${itemData}">
<div class="card" style="width: 12rem;">
  <a href="ItemDetail?id=${item.id}"><img src="img/${item.imgFile}"  class="card-img-top"alt=""></a>
  <div class="card-body">
    <div class="row">
    <p class="card-text" >${item.name}</p>
    </div>

    <div class="row" style=" margin-top:50px; margin-left:120px;">
    <p>${item.price}円</p>
    </div>
  </div>
</div>
</c:forEach>

</div>
<!--ページネイション-->
<!--
 <nav aria-label="Page navigation">
  <ul class="pagination justify-start" >
    <li class="page-item"><a class="page-link" href="#">Prev</a></li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item"><a class="page-link" href="#">Next</a></li>
  </ul>
</nav>  -->
<!--ページネイション-->

</div>
<!--商品一覧画面!-->


</div>

</div> <!--/container !-->

    <footer class="sticky-Footer">
     <p style="text-align: center;"> © 2020 EC </p>
    </footer>

  </body>
</html>